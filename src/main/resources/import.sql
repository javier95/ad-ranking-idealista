INSERT INTO AD (id, description, garden_size, house_size, irrelevant_since, score, typology) VALUES (1, 'Este piso es una ganga, compra, compra, COMPRA!!!!!', 200, 300, null, null, 'CHALET');
INSERT INTO AD (id, description, garden_size, house_size, irrelevant_since, score, typology) VALUES (2, 'Nuevo ático céntrico recién reformado. No deje pasar la oportunidad y adquiera este ático de lujo', null, 300, null, null, 'FLAT');
INSERT INTO AD (id, description, garden_size, house_size, irrelevant_since, score, typology) VALUES (3, '', null, 300, null, null, 'CHALET');
INSERT INTO AD (id, description, garden_size, house_size, irrelevant_since, score, typology) VALUES (4, 'Ático céntrico muy Luminoso, y recién reformado, parece nuevo', null, 300, null, null, 'FLAT');
INSERT INTO AD (id, description, garden_size, house_size, irrelevant_since, score, typology) VALUES (5, 'Pisazo,', null, 300, null, null, 'FLAT');
INSERT INTO AD (id, description, garden_size, house_size, irrelevant_since, score, typology) VALUES (6, '', null, 300, null, null, 'GARAGE');
INSERT INTO AD (id, description, garden_size, house_size, irrelevant_since, score, typology) VALUES (7, 'Garaje en el centro de Albacete', null, 300, null, null, 'GARAGE');
INSERT INTO AD (id, description, garden_size, house_size, irrelevant_since, score, typology) VALUES (8, 'Maravilloso chalet situado en lAs afueras de un pequeño pueblo rural. El entorno es espectacular, las vistas magníficas. ¡Cómprelo ahora!', null, 300, null, null, 'CHALET');


INSERT INTO PICTURE (id, quality, url) VALUES (1, 'SD', 'http://www.idealista.com/pictures/1');
INSERT INTO PICTURE (id, quality, url) VALUES (2, 'HD', 'http://www.idealista.com/pictures/2');
INSERT INTO PICTURE (id, quality, url) VALUES (3, 'SD', 'http://www.idealista.com/pictures/3');
INSERT INTO PICTURE (id, quality, url) VALUES (4, 'HD', 'http://www.idealista.com/pictures/4');
INSERT INTO PICTURE (id, quality, url) VALUES (5, 'SD', 'http://www.idealista.com/pictures/5');
INSERT INTO PICTURE (id, quality, url) VALUES (6, 'SD', 'http://www.idealista.com/pictures/6');
INSERT INTO PICTURE (id, quality, url) VALUES (7, 'SD', 'http://www.idealista.com/pictures/7');
INSERT INTO PICTURE (id, quality, url) VALUES (8, 'HD', 'http://www.idealista.com/pictures/8');

INSERT INTO AD_PICTURES (ad_id, pictures_id) VALUES (1, 1);
INSERT INTO AD_PICTURES (ad_id, pictures_id) VALUES (2, 4);
INSERT INTO AD_PICTURES (ad_id, pictures_id) VALUES (3, 2);
INSERT INTO AD_PICTURES (ad_id, pictures_id) VALUES (4, 5);
INSERT INTO AD_PICTURES (ad_id, pictures_id) VALUES (5, 3);
INSERT INTO AD_PICTURES (ad_id, pictures_id) VALUES (5, 8);
INSERT INTO AD_PICTURES (ad_id, pictures_id) VALUES (6, 6);
INSERT INTO AD_PICTURES (ad_id, pictures_id) VALUES (8, 7);
INSERT INTO AD_PICTURES (ad_id, pictures_id) VALUES (8, 1);
