package com.idealista.infrastructure.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class InMemoryPersistence {

    @Autowired
    private EntityManager entityManager;

    /**
     * Método para obtener de base de datos una fotografía concreta por su URL
     *
     * @param url
     * @return PictureVO
     * @throws Exception
     */
    public PictureVO findPictureByUrl(String url) throws Exception {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<PictureVO> query = cb.createQuery(PictureVO.class);
            Root<PictureVO> picture = query.from(PictureVO.class);
            query.select(picture).where(picture.get("url").in(url));
            return entityManager.createQuery(query).getSingleResult();
    }

    /**
     * Método para modificar un anuncio
     *
     * @param modifiedAdVo
     * @throws Exception
     */
    @Transactional
    public void modifyAdVo(AdVO modifiedAdVo)  throws Exception{
            entityManager.createNativeQuery("UPDATE AD SET description = ?, garden_size = ?, house_size = ?," +
                    "irrelevant_since = ?, score = ?, typology = ? WHERE id = ?")
                    .setParameter(1, modifiedAdVo.getDescription())
                    .setParameter(2, modifiedAdVo.getGardenSize())
                    .setParameter(3, modifiedAdVo.getHouseSize())
                    .setParameter(4, modifiedAdVo.getIrrelevantSince())
                    .setParameter(5, modifiedAdVo.getScore())
                    .setParameter(6, modifiedAdVo.getTypology())
                    .setParameter(7, modifiedAdVo.getId())
                    .executeUpdate();
            entityManager.close();
    }

    /**
     * Método para obtener el listado de los anuncios de la base de datos
     *
     * @return List<AdVO>
     * @throws Exception
     */
    public List<AdVO> listAds()  throws Exception{
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<AdVO> query = cb.createQuery(AdVO.class);
        Root<AdVO> ad = query.from(AdVO.class);
        query.select(ad);
        return entityManager.createQuery(query).getResultList();
    }

    /**
     * Método para obtener el listado de los anuncios de la base de datos, ordenado segun la puntuacion,
     * de mas alto a mas bajo
     *
     * @return List<AdVO>
     * @throws Exception
     */
    public List<AdVO> listAdsOrderByScore()  throws Exception{
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<AdVO> query = cb.createQuery(AdVO.class);
        Root<AdVO> ad = query.from(AdVO.class);
        query.select(ad).orderBy(cb.desc(ad.get("score")));
        return entityManager.createQuery(query).getResultList();
    }
}
