package com.idealista.infrastructure.api.models.mappers;

import com.idealista.infrastructure.api.models.QualityAd;
import com.idealista.infrastructure.persistence.AdVO;
import com.idealista.infrastructure.persistence.InMemoryPersistence;
import com.idealista.infrastructure.persistence.PictureVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Interfaz para mapear la entidad AdVO al DTO QualityAd
 * Se utiliza mapstruct para simplificar la tarea de implementación de los mappers.
 */
@Mapper
public abstract class IQualityAdMapper {

    @Autowired
    InMemoryPersistence inMemoryPersistence;

    @Mapping(target = "pictureUrls", source = "pictures", qualifiedByName = "pictureToPictureUrl")
    public abstract QualityAd entityToDto(AdVO entity);

    @Mapping(target = "pictures", source = "pictureUrls", qualifiedByName = "picturesUrlToPicture")
    public abstract AdVO dtoToEntity(QualityAd dto);

    public abstract List<QualityAd> entitiesToDto(List<AdVO> entities);

    public abstract List<AdVO> dtoToEntities(List<QualityAd> dto);

    /**
     * Método para mepar la URL de una fotografía con el identificador de esa fotografía
     * @param picturesUrls
     * @return List<PictureVO>
     */
    @Named("picturesUrlToPicture")
    List<PictureVO> picturesUrlToPicture(List<String> picturesUrls) {
        List<PictureVO> pictures = new ArrayList<>();
        try {
            if (picturesUrls != null) {
                for (String url : picturesUrls) {
                    pictures.add(inMemoryPersistence.findPictureByUrl(url));
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
           return new ArrayList<>();
        }

        return pictures;
    }

    /**
     * Método para mepar el identificador de una fotografía con la URL de esa fotografía
     * @param pictures
     * @return List<String>
     */
    @Named("pictureToPictureUrl")
    List<String> pictureToPictureUrl(List<PictureVO> pictures) {
        List<String> picturesUrl = new ArrayList<>();
        if (pictures != null) {
            for (PictureVO picture : pictures) {
                picturesUrl.add(picture.getUrl());
            }
        }
        return picturesUrl;
    }
}
