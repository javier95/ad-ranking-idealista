package com.idealista.infrastructure.api.controllers;

import com.idealista.infrastructure.api.models.PublicAd;
import com.idealista.infrastructure.api.models.QualityAd;
import com.idealista.infrastructure.api.services.IAdsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/ads")
@Controller
@Api(description = "Controlador para exponer los enpoints relacionados con los anuncios de la plataforma")
public class AdsController {

    @Autowired
    private IAdsService adsService;

    @GetMapping("/qualityListing")
    @ApiOperation(value = "Enpoint para obtener el listado de los anuncios junto con la puntuacion")
    public ResponseEntity<List<QualityAd>> qualityListing() {
        return adsService.qualityListing();
    }

    @ApiOperation(value = "Enpoint para obtener el listado de los anuncios ordenados de puntuacion mas alta a mas baja")
    @GetMapping("/publicListing")
    public ResponseEntity<List<PublicAd>> publicListing() {
        return adsService.publicListing();
    }
    @ApiOperation(value = "Enpoint para calcular la puntuacion de cada uno de los anuncios")
    @GetMapping("/calculateScore")
    public ResponseEntity<Void> calculateScore() {
        return adsService.calculateScore();
    }
}
