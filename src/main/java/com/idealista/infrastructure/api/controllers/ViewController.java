package com.idealista.infrastructure.api.controllers;

import com.idealista.infrastructure.api.services.IAdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Controlador para manejar las vistas del sistema.
 * Ofrece el punto de entrada a la parte grafica de la plataforma y la redireccion al resto de paginas.
 */
@Controller
@ApiIgnore
public class ViewController {

    @Autowired
    private IAdsService adsService;

    @GetMapping("/home")
    public String home() {
        return "home";
    }

    @PostMapping("/view")
    public String view(Model model, @RequestParam String tipo) {
        if (tipo.equals("usuario")) {
            model.addAttribute("ads", adsService.publicListing().getBody());
        } else if (tipo.equals("encargado")) {
            model.addAttribute("ads", adsService.qualityListing().getBody());
        }
        model.addAttribute("tipo", tipo);
        return "view";
    }
}
