package com.idealista.infrastructure.api.services.impl;

import com.idealista.infrastructure.api.models.PublicAd;
import com.idealista.infrastructure.api.models.QualityAd;
import com.idealista.infrastructure.api.models.mappers.IPublicAdMapper;
import com.idealista.infrastructure.api.models.mappers.IQualityAdMapper;
import com.idealista.infrastructure.api.services.IAdsService;
import com.idealista.infrastructure.persistence.AdVO;
import com.idealista.infrastructure.persistence.InMemoryPersistence;
import com.idealista.infrastructure.persistence.PictureVO;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class AdsServiceImpl implements IAdsService {

    /**
     * Estos parametros estaticos se podrian sacar a un enumerado en el caso de que se necesiten fuera de esta clase
     */
    private static final String HD = "HD";
    private static final String SD = "SD";
    private static final String FLAT = "FLAT";
    private static final String CHALET = "CHALET";


    @Autowired
    private InMemoryPersistence inMemoryPersistence;

    /**
     * Método para recuperar los anuncios, en el formato QualityAd.
     * Este formato incluye la puntuacion y si es un anuncio irrelevante.
     * Es el formato que se utiliza para mostrar la informacion al encargado.
     *
     * @return ResponseEntity<List < QualityAd>>
     */
    @Override
    public ResponseEntity<List<QualityAd>> qualityListing() {
        try {
            return new ResponseEntity<>(Mappers.getMapper(IQualityAdMapper.class)
                    .entitiesToDto(inMemoryPersistence.listAds()), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * Método para recuperar los anuncios, en el formato PublicAd.
     * Es el formato que se utiliza para mostrar la informacion al usuario.
     *
     * @return ResponseEntity<List < PublicAd>>
     */
    @Override
    public ResponseEntity<List<PublicAd>> publicListing() {
        try {
            return new ResponseEntity<>(Mappers.getMapper(IPublicAdMapper.class)
                    .entitiesToDto(inMemoryPersistence.listAdsOrderByScore()), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * Este método se encarga de realizar la logica necesaria para calcula la puntuación
     * de cada uno de los anuncios de base de datos.
     * Este logica implmentada sigue los requitos definidos en el documento
     *
     * @return ResponseEntity<Void>
     */
    @Override
    public ResponseEntity<Void> calculateScore() {
        try {
            for (AdVO adVo : inMemoryPersistence.listAds()) {
                adVo.setScore(0);
                if (adVo.getDescription() != null && !adVo.getDescription().isEmpty()) {
                    this.plusScore(adVo, 5);
                }
                this.descriptionsScore(adVo);
                this.keyWordsScore(adVo);
                this.adCompleteScore(adVo);
                this.picturesScore(adVo);
                this.irrelevantVerification(adVo);
                if (adVo.getScore() < 0) {
                    adVo.setScore(0);
                } else if (adVo.getScore() > 100) {
                    adVo.setScore(100);
                }
                inMemoryPersistence.modifyAdVo(adVo);
            }
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Método para calcular la puntuacion de un anuncio, para el parametro de las fotografias
     *
     * @param adVo
     */
    private void picturesScore(AdVO adVo) {
        if (adVo.getPictures() != null && !adVo.getPictures().isEmpty()) {
            for (PictureVO picture : adVo.getPictures()) {
                if (picture.getQuality().equals(HD)) {
                    this.plusScore(adVo, 20);
                } else if (picture.getQuality().equals(SD)) {
                    this.plusScore(adVo, 10);
                }
            }
        } else {
            this.plusScore(adVo, -10);
        }
    }

    /**
     * Método para calcular la puntuacion de un anuncio, para el parametro de descripcion
     *
     * @param adVo
     */
    private void descriptionsScore(AdVO adVo) {
        if (adVo.getTypology() != null) {
            if (adVo.getTypology().equals(FLAT)) {
                if (adVo.getDescription() != null) {
                    if (this.descriptionWords(adVo.getDescription()).size() >= 20 && this.descriptionWords(adVo.getDescription()).size() < 50) {
                        this.plusScore(adVo, 10);
                    } else if (this.descriptionWords(adVo.getDescription()).size() > 50) {
                        this.plusScore(adVo, 30);
                    }
                }

            } else if (adVo.getTypology().equals(CHALET)) {
                if (adVo.getDescription() != null && this.descriptionWords(adVo.getDescription()).size() > 50) {
                    this.plusScore(adVo, 20);
                }
            }
        }

    }

    /**
     * Método para calcular la puntuacion de un anuncio, segun las palabras clave que contenga la descripción
     * @param adVo
     */
    private void keyWordsScore(AdVO adVo) {
        if (adVo.getDescription() != null && !adVo.getDescription().isEmpty()) {
            for (String key : this.descriptionWords(adVo.getDescription())) {
                //En el caso que la palabra seleccionada contegna una coma, se elimina para posteriormente realizar la comparativa con las palabras clave.
                if (key.lastIndexOf(",") != -1) {
                    key = key.substring(0, key.lastIndexOf(","));
                }
                if (key.toLowerCase().equals("luminoso") ||
                        key.toLowerCase().equals("nuevo") ||
                        key.toLowerCase().equals("céntrico") ||
                        key.toLowerCase().equals("reformado") ||
                        key.toLowerCase().equals("ático")) {
                    this.plusScore(adVo, 5);
                }
            }
        }
    }

    /**
     * Método para calcular la puntuacion de un anuncio, segun si el anuncio esta completo o no.
     *
     * @param adVo
     */
    private void adCompleteScore(AdVO adVo) {
        if (adVo.getTypology() != null) {
            if (adVo.getTypology().equals(FLAT)) {
                if (adVo.getDescription() != null &&
                        !adVo.getDescription().isEmpty() &&
                        adVo.getPictures() != null &&
                        adVo.getPictures().size() >= 1 &&
                        adVo.getHouseSize() != null) {
                    this.plusScore(adVo, 40);
                }
            } else if (adVo.getTypology().equals(CHALET)) {
                if (adVo.getDescription() != null &&
                        !adVo.getDescription().isEmpty() &&
                        adVo.getPictures() != null &&
                        adVo.getPictures().size() >= 1 &&
                        adVo.getHouseSize() != null &&
                        adVo.getGardenSize() != null) {
                    this.plusScore(adVo, 40);
                }
            }
        }
    }

    /**
     * Método para comprobar si un anuncio es irrelevante y establecer la fecha.
     *
     * @param adVo
     */
    private void irrelevantVerification(AdVO adVo) {
        if (adVo.getScore() != null) {
            if (adVo.getScore() < 40) {
                adVo.setIrrelevantSince(new Date());
            }
        }

    }

    /**
     * Metodo encargado de sumar los puntos en un anuncio
     *
     * @param adVo
     * @param points
     */
    private void plusScore(AdVO adVo, Integer points) {
        adVo.setScore(adVo.getScore() + points);
    }

    /**
     * Método para separar cada una de las palabras que contiene la descripcion
     * @param description
     * @return List<String>
     */
    private List<String> descriptionWords(String description) {
        return Arrays.asList(description.split(" "));
    }
}
