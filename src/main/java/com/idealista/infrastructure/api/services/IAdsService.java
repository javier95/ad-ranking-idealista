package com.idealista.infrastructure.api.services;

import com.idealista.infrastructure.api.models.PublicAd;
import com.idealista.infrastructure.api.models.QualityAd;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IAdsService {

    ResponseEntity<List<QualityAd>> qualityListing();
    ResponseEntity<List<PublicAd>> publicListing();
    ResponseEntity<Void> calculateScore();
}
