package test;


import com.idealista.infrastructure.api.models.PublicAd;
import com.idealista.infrastructure.api.models.QualityAd;
import com.idealista.infrastructure.api.services.IAdsService;
import com.idealista.infrastructure.api.services.impl.AdsServiceImpl;
import com.idealista.infrastructure.persistence.AdVO;
import com.idealista.infrastructure.persistence.InMemoryPersistence;
import com.idealista.infrastructure.persistence.PictureVO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {AdsServiceImpl.class, InMemoryPersistence.class})
public class AdsServiceTest {

    @Autowired
    private IAdsService adsService;
    @MockBean
    private InMemoryPersistence inMemoryPersistence;

    @Test
    public void qualityListingTest() throws Exception {
        AdVO adVoTest = new AdVO();
        adVoTest.setDescription("Descripción de prueba");
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenReturn(mockResponse);
        ResponseEntity<List<QualityAd>> result = adsService.qualityListing();

        verify(inMemoryPersistence).listAds();
        Assert.assertEquals(Integer.valueOf(1), result.getBody().get(0).getId());
        Assert.assertEquals(Integer.valueOf(200), result.getBody().get(0).getHouseSize());
        Assert.assertEquals("Descripción de prueba", result.getBody().get(0).getDescription());
    }

    @Test
    public void qualityListingTestKo() throws Exception {
        AdVO adVoTest = new AdVO();
        adVoTest.setDescription("Descripción de prueba");
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenThrow(Exception.class);
        ResponseEntity<List<QualityAd>> result = adsService.qualityListing();

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
    }

    @Test
    public void publicListingTest() throws Exception {
        AdVO adVoTest = new AdVO();
        adVoTest.setDescription("Descripción de prueba");
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAdsOrderByScore()).thenReturn(mockResponse);
        ResponseEntity<List<PublicAd>> result = adsService.publicListing();

        verify(inMemoryPersistence).listAdsOrderByScore();
        Assert.assertEquals(Integer.valueOf(1), result.getBody().get(0).getId());
        Assert.assertEquals(Integer.valueOf(200), result.getBody().get(0).getHouseSize());
        Assert.assertEquals("Descripción de prueba", result.getBody().get(0).getDescription());
    }

    @Test
    public void publicListingTestKo() throws Exception {
        AdVO adVoTest = new AdVO();
        adVoTest.setDescription("Descripción de prueba");
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAdsOrderByScore()).thenThrow(Exception.class);
        ResponseEntity<List<PublicAd>> result = adsService.publicListing();

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
    }

    @Test
    public void calculateScoreTestOk() throws Exception {
        List<PictureVO> picturesTest = new ArrayList<>();
        picturesTest.add(new PictureVO(1, "http://url", "HD"));
        AdVO adVoTest = new AdVO();
        adVoTest.setDescription("Descripción de prueba, piso luminoso");
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setPictures(picturesTest);
        adVoTest.setTypology("FLAT");
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenReturn(mockResponse);
        ResponseEntity<Void> result = adsService.calculateScore();

        verify(inMemoryPersistence).listAds();
        Assert.assertEquals(HttpStatus.OK,result.getStatusCode());
    }

    @Test
    public void calculateScoreTestOkChalet() throws Exception {
        List<PictureVO> picturesTest = new ArrayList<>();
        picturesTest.add(new PictureVO(1, "http://url", "HD"));
        AdVO adVoTest = new AdVO();
        adVoTest.setDescription("Descripción de prueba, luminoso, nuevo y con terraza");
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setPictures(picturesTest);
        adVoTest.setTypology("CHALET");
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenReturn(mockResponse);
        ResponseEntity<Void> result = adsService.calculateScore();

        verify(inMemoryPersistence).listAds();
        Assert.assertEquals(HttpStatus.OK,result.getStatusCode());
    }

    @Test
    public void calculateScoreTestOkPictureSd() throws Exception {
        List<PictureVO> picturesTest = new ArrayList<>();
        picturesTest.add(new PictureVO(1, "http://url", "SD"));
        AdVO adVoTest = new AdVO();
        adVoTest.setDescription("Descripción de prueba");
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setPictures(picturesTest);
        adVoTest.setTypology("FLAT");
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenReturn(mockResponse);
        ResponseEntity<Void> result = adsService.calculateScore();

        verify(inMemoryPersistence).listAds();
        Assert.assertEquals(HttpStatus.OK,result.getStatusCode());
    }

    @Test
    public void calculateScoreTestOkNoPictures() throws Exception {
        AdVO adVoTest = new AdVO();
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setTypology("FLAT");
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenReturn(mockResponse);
        ResponseEntity<Void> result = adsService.calculateScore();

        verify(inMemoryPersistence).listAds();
        Assert.assertEquals(HttpStatus.OK,result.getStatusCode());
    }

    @Test
    public void calculateScoreTestOkBigScore() throws Exception {
        List<PictureVO> picturesTest = new ArrayList<>();
        picturesTest.add(new PictureVO(1, "http://url/1", "HD"));
        picturesTest.add(new PictureVO(2, "http://url/2", "HD"));
        picturesTest.add(new PictureVO(3, "http://url/3", "HD"));
        picturesTest.add(new PictureVO(4, "http://url/4", "HD"));
        picturesTest.add(new PictureVO(5, "http://url/5", "HD"));
        picturesTest.add(new PictureVO(6, "http://url/6", "HD"));
        AdVO adVoTest = new AdVO();
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setPictures(picturesTest);
        adVoTest.setTypology("FLAT");
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenReturn(mockResponse);
        ResponseEntity<Void> result = adsService.calculateScore();

        verify(inMemoryPersistence).listAds();
        Assert.assertEquals(HttpStatus.OK,result.getStatusCode());
    }

    @Test
    public void calculateScoreTestOkLowScore() throws Exception {
        List<PictureVO> picturesTest = new ArrayList<>();
        picturesTest.add(new PictureVO(1, "http://url", "HD"));
        AdVO adVoTest = new AdVO();
        adVoTest.setGardenSize(200);
        adVoTest.setScore(-5);
        adVoTest.setHouseSize(200);
        adVoTest.setPictures(picturesTest);
        adVoTest.setTypology("FLAT");
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenReturn(mockResponse);
        ResponseEntity<Void> result = adsService.calculateScore();

        verify(inMemoryPersistence).listAds();
        Assert.assertEquals(HttpStatus.OK,result.getStatusCode());
    }

    @Test
    public void calculateScoreTestOkLongDescriptionChalet() throws Exception {
        AdVO adVoTest = new AdVO();
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setDescription("Bonito y cómodo apartamento amueblado.\n" +
                "Bonito y cómodo apartamento de 1 habitación con cama doble, un baño y salón comedor," +
                " se puede ir caminando a Plaza Mayor, Paseo del Prado, Sol, entre otros sitios turísticos de interés\n" +
                "Cuenta con una cocina totalmente equipada, aire acondicionado, lavadora, tv.\n" +
                "El apartamento tiene una ubicación privilegiada, esta cerca de la zona de los museos, teatros, restaurantes" +
                " y tiendas de moda conocidas.");
        adVoTest.setTypology("CHALET");
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenReturn(mockResponse);
        ResponseEntity<Void> result = adsService.calculateScore();

        verify(inMemoryPersistence).listAds();
        Assert.assertEquals(HttpStatus.OK,result.getStatusCode());
    }

    @Test
    public void calculateScoreTestOkLongDescription() throws Exception {
        AdVO adVoTest = new AdVO();
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setDescription("Bonito y cómodo apartamento amueblado.\n" +
                "Bonito y cómodo apartamento de 1 habitación con cama doble, un baño y salón comedor," +
                " se puede ir caminando a Plaza Mayor, Paseo del Prado, Sol, entre otros sitios turísticos de interés\n" +
                "Cuenta con una cocina totalmente equipada, aire acondicionado, lavadora, tv.\n" +
                "El apartamento tiene una ubicación privilegiada, esta cerca de la zona de los museos, teatros, restaurantes" +
                " y tiendas de moda conocidas.");
        adVoTest.setTypology("FLAT");
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenReturn(mockResponse);
        ResponseEntity<Void> result = adsService.calculateScore();

        verify(inMemoryPersistence).listAds();
        Assert.assertEquals(HttpStatus.OK,result.getStatusCode());
    }

    @Test
    public void calculateScoreTestOkMediumDescription() throws Exception {
        AdVO adVoTest = new AdVO();
        adVoTest.setGardenSize(200);
        adVoTest.setScore(-5);
        adVoTest.setDescription("Bonito y cómodo apartamento amueblado.\n" +
                "Bonito y cómodo apartamento de 1 habitación con cama doble, un baño y salón comedor," +
                " se puede ir caminando a Plaza Mayor, Paseo del Prado, Sol, entre otros sitios turísticos de interés\n");
        adVoTest.setHouseSize(200);
        adVoTest.setTypology("FLAT");
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenReturn(mockResponse);
        ResponseEntity<Void> result = adsService.calculateScore();

        verify(inMemoryPersistence).listAds();
        Assert.assertEquals(HttpStatus.OK,result.getStatusCode());
    }


    @Test
    public void calculateScoreTestKo() throws Exception {
        AdVO adVoTest = new AdVO();
        adVoTest.setDescription("Descripción de prueba");
        adVoTest.setGardenSize(200);
        adVoTest.setHouseSize(200);
        adVoTest.setId(1);

        List<AdVO> mockResponse = new ArrayList<>();
        mockResponse.add(adVoTest);

        when(inMemoryPersistence.listAds()).thenThrow(Exception.class);
        ResponseEntity<Void> result = adsService.calculateScore();

        verify(inMemoryPersistence).listAds();
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,result.getStatusCode());
    }
}
