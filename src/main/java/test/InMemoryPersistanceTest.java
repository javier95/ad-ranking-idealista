package test;

import com.idealista.Main;
import com.idealista.infrastructure.persistence.AdVO;
import com.idealista.infrastructure.persistence.InMemoryPersistence;
import com.idealista.infrastructure.persistence.PictureVO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Main.class)
public class InMemoryPersistanceTest {

    @Autowired
    private InMemoryPersistence inMemoryPersistence;


    @Test
    public void findPictureByUrl() throws Exception {

        PictureVO pictureTest = new PictureVO();
        pictureTest.setId(1);
        pictureTest.setQuality("SD");

        PictureVO response = inMemoryPersistence.findPictureByUrl("http://www.idealista.com/pictures/1");

        assertNotNull(response);
        Assert.assertEquals(Integer.valueOf(1), response.getId());
        Assert.assertEquals("SD", response.getQuality());
    }

    @Test
    public void listAdsTest() throws Exception {

        List<AdVO> response = inMemoryPersistence.listAds();

        Assert.assertEquals(8, response.size());
        Assert.assertEquals("CHALET", response.get(7).getTypology());
    }

    @Test
    public void listAdsOrderByScoreTest() throws Exception {

        List<AdVO> response = inMemoryPersistence.listAdsOrderByScore();

        assertNotNull(response);
        Assert.assertEquals(8, response.size());
    }

}
